package simulation

import cargo.Item
import rocket.Rocket
import rocket.U1
import rocket.U2
import java.io.File

class Simulation {
    fun loadItems(fileName: String): ArrayList<Item>{
        val items = arrayListOf<Item>()

        File(fileName).forEachLine {
            val line = it.split("=")
            items.add(Item(line[0], line[1].toInt()/1000))
        }

        return items
    }

    fun loadU1(items: ArrayList<Item>): ArrayList<U1>{
        val rockets = arrayListOf<U1>()
        val sortedItems = arrayListOf<Item>()
        sortedItems.addAll(items.sortedByDescending { it.weight })

        while (sortedItems.isNotEmpty()){
            val rocket = U1()

            for (item in sortedItems)
                if (rocket.isFull)
                    break
                else
                    if (rocket.canCarry(item)) rocket.carry(item)

            rockets.add(rocket)
            sortedItems.removeAll(rocket.cargo)
        }

        return rockets
    }

    fun loadU2(items: ArrayList<Item>): ArrayList<U2>{
        val rockets = arrayListOf<U2>()
        val sortedItems = arrayListOf<Item>()
        sortedItems.addAll(items.sortedByDescending { it.weight })

        while (sortedItems.isNotEmpty()){
            val rocket = U2()

            for (item in sortedItems)
                if (rocket.isFull)
                    break
                else
                    if (rocket.canCarry(item)) rocket.carry(item)

            rockets.add(rocket)
            sortedItems.removeAll(rocket.cargo)
        }

        return rockets
    }

    fun<T: Rocket> runSimulation(rockets: ArrayList<T>): Long{
        var budget = 0L

        rockets.forEachIndexed { index, rocket ->
            do {
                if (shouldHaveShield(index)) rocket.addShield()
                while (!rocket.launch()) budget += rocket.cost
                budget += rocket.cost
            } while (!rocket.land())

            if (shouldHaveShield(index)) println("Rocket #${index+1} with active shield has landed")
        }

        return budget
    }
}

fun Rocket.addShield() = println("Shield added to ${this::class.simpleName} Rocket")
fun shouldHaveShield(index: Int) = (index+1).rem(3) == 0
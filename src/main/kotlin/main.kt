import simulation.Simulation
import java.text.DecimalFormat

fun main(args: Array<String>) {
    val phase1 = "src/main/resources/phase-1.txt"
    val phase2 = "src/main/resources/phase-2.txt"
    val simulation = Simulation()

    val itemsPhase1 = simulation.loadItems(phase1)
    val itemsPhase2 = simulation.loadItems(phase2)

    val u1Phase1 = simulation.loadU1(itemsPhase1)
    val u1Phase2 = simulation.loadU1(itemsPhase2)

    val u2Phase1 = simulation.loadU2(itemsPhase1)
    val u2Phase2 = simulation.loadU2(itemsPhase2)

    val formatter = DecimalFormat("#,###.00")

    println("Phase 1 budget for ${u1Phase1.size} U1 rockets: $${formatter.format(simulation.runSimulation(u1Phase1))}\n")
    println("Phase 2 budget for ${u1Phase2.size} U1 rockets: $${formatter.format(simulation.runSimulation(u1Phase2))}\n")
    println("Phase 1 budget for ${u2Phase1.size} U2 rockets: $${formatter.format(simulation.runSimulation(u2Phase1))}\n")
    println("Phase 2 budget for ${u2Phase2.size} U2 rockets: $${formatter.format(simulation.runSimulation(u2Phase2))}")
}
package rocket

import cargo.Item

open class Rocket(val cost: Int, val weight: Int, val maxWeight: Int): SpaceShip {
    val cargoLimit = maxWeight - weight
    val cargo = mutableListOf<Item>()
    val isFull get() = loadedCargo() == cargoLimit
    val isEmpty get() = loadedCargo() == 0

    override fun launch(): Boolean = true
    override fun land(): Boolean = true
    override fun canCarry(item: Item): Boolean = loadedCargo() + item.weight <= cargoLimit
    override fun carry(item: Item) { cargo.add(item) }
    fun loadedCargo() = cargo.sumOf { it.weight }
}
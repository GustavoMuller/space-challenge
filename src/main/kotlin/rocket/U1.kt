package rocket

import java.util.*

class U1(cost: Int = 100_000_000, weight: Int = 10, maxWeight: Int = 18): Rocket(cost, weight, maxWeight) {

    override fun launch(): Boolean = if(isEmpty) true else Random().nextDouble() > (0.05 * loadedCargo().div(cargoLimit.toDouble()))
    override fun land(): Boolean = if(isEmpty) true else Random().nextDouble() > (0.01 * loadedCargo().div(cargoLimit.toDouble()))

}
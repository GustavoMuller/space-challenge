package rocket

import java.util.*

class U2(cost: Int = 120_000_000, weight: Int = 18, maxWeight: Int = 29): Rocket(cost, weight, maxWeight) {

    override fun launch(): Boolean = if(isEmpty) true else Random().nextDouble() > (0.04 * loadedCargo().div(cargoLimit.toDouble()))
    override fun land(): Boolean = if(isEmpty) true else Random().nextDouble() > (0.08 * loadedCargo().div(cargoLimit.toDouble()))

}